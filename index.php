<!DOCTYPE html>
<html>
<head>
<title>CUPS Web Printing</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="./cwp-style.css">
</head>

<body>

<h1>CUPS Web Printing</h1>

<?php
  // functions file
  require "./cwp-functions.php";
  $settings = require './config/settings.php';
  // test gettext
  bindtextdomain('messages','./locales');
  setlocale(LC_ALL, "fr_FR.UTF-8");
  textdomain('messages');
  Main($settings);
?>

</body>

</html>
