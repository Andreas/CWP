<?php

/*
 * Copyright 2016, Yvan Masson
 *
 * This file is part of CWP.
 *
 * CWP is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.

 * CWP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with CWP.  If not, see <http://www.gnu.org/licenses/>.
*/

require('./utils.php');

// main function
function Main (array $settings)
{
    // create global <div>
    echo '<div id="cwp_div">';

    // check variables defined by admin
    CheckAdminVariables($settings);

    // read printers and strings lists
    $printers_content = require($settings["printers_file"]);
    $strings_content = require($settings["strings_file"]);

    // check printers definition against possible admin error
    CheckPrinters($printers_content);

    // get browser preferred language
    $settings["user_lang"] = GetLanguageCodeISO6391($settings["default_lang"],
                                                    $settings["debug"]);

    // eventually print and display result
    if (!empty($_POST)) {

        // delete annoying $_POST[$MAX_FILE_SIZE] (we can acces $settings["max_file_size"])
        unset($_POST["MAX_FILE_SIZE"]);
        // make a copy of parameters to ease the job
        $user_input = $_POST;

        // check uploaded file and $_POST
        if (IsFILESValid($settings, $strings_content["strings"])) {
            if (!IsPOSTValid($printers_content["printers"], $user_input)) {
                echo '<p class="cwp_error">Invalid input</p>';
            } else {
                // check known text parameters
                if (CheckUserStrings($settings, $strings_content["strings"], $user_input)) {
                    // send to print
                    PreparePrinting($settings, $strings_content["strings"], $user_input);
                }
            }
        }
    }

    // display the printing forms
    GenForms($settings,
             $printers_content["printers"],
             $strings_content["strings"]);

    // close global div
    echo '</div>';
}

// check admin defined variables
function CheckAdminVariables (array $settings)
{
    $validation_rules = [
        'printers_file' => 'required',
        'strings_file' => 'required',
        'default_lang' => 'required|regex:/^[a-z]{2,3}$/',
        'retrieve_printers_status' => 'required|boolean',
        'show_printers_status' => 'required|boolean',
        'disable_faulty_printers' => 'required|boolean',
        'max_copies' => 'required|integer|min:0',
        'max_file_size' => 'required|integer|min:0',
        'default_cups_server' => 'url'

    ];

    $errors = validateArray($settings, $validation_rules);

    if(!isset($settings['default_cups_server'])) {
        $settings['default_cups_server'] = 'localhost';
    }

    //@todo revoir l'affichage des erreurs quand on aura des templates
    if(count($errors) > 0) {
        var_dump($errors);
    }
}

// check if a cups printer or class name is valid
function IsValidPrinterName ($printer) {
    // see man page of lpadmin
    // TODO could not find a regex to validate "all printable unicode
    // characters but spaces, "#" and "/".
    return true;
}

// check printers JSON file against possible problems
function CheckPrinters (array $printers_content)
{
    // check array root
    $validation_rules = [
        '' => 'count:1',
        'printers' => 'required|minCount:1|indexed'
    ];

    $errors = validateArray($printers_content, $validation_rules);
    if(count($errors) > 0) {
        var_dump($errors);
    }

    // array to find duplicate printers
    $tempArray;

    // for each printer
    foreach ($printers_content["printers"] as $printer) {
        $validation_rules = [
            '' => 'associative',
            'cups-name' => 'required|printerName',
            'server' => 'required|serverUri',
            'name' => 'associative',
            'name|keys' => 'regex:/^[a-z]{2,3}$/',
            'name|values' => 'string',
            'location' => 'associative',
            'location|keys' => 'regex:/^[a-z]{2,3}$/',
            'location|values' => 'string',
            'link' => 'url',
            'options' => 'associative',
            'options|keys' => 'regex:/^([a-zA-Z]|\ |\.|\-)+$/',
            'options|values' => 'associative'
        ];
        $errors = validateArray($printer, $validation_rules);
        if(count($errors) > 0) {
            var_dump($errors);
        }

        // populate the array to find duplicate printers
        // printer name and DNS are case-insensitive
        $tempArray[] = mb_strtolower($printer["cups-name"])
                        . '@' . mb_strtolower($printer["server"]);

        // continue if there is no option
        if (!isset($printer["options"])) {
            continue;
        }

        // for each option
        foreach ($printer["options"] as $cups_option_name => $option) {
            $opt_validation_rules = [];
            // check values (list or boolean option)
            if (isset($option["values"])) {
                // values can be the string "bool"
                if (is_string($option["values"])) {
                    $opt_validation_rules['values'] = 'regex:/bool/';
                    $opt_validation_rules['default'] = 'boolean';
                }
                // or an indexed array
                else {
                    $opt_validation_rules['values'] = 'indexed';
                    // each value must be a valid string
                    // TODO filter is only based on my personnal printers...
                    $opt_validation_rules['values|values'] = 'regex:/^([a-zA-Z]|\d|\.|\-)+$/';
                    // Ensure that the default value corresponds to a defined value
                    $opt_validation_rules['default'] = function($field) use ($option) {
                        return !isset($field) || in_array($field, $option["values"]) === true ? '' : _('is not a member of values');
                    };
                }
            }

            $opt_validation_rules['changeable'] = 'boolean';
            $errors = validateArray($option, $opt_validation_rules);
            if(count($errors) > 0) {
                var_dump($errors);
            }
        }
    }

    // finally check if a printer is declared more than one time
    if(count(array_unique($tempArray)) < count($tempArray)) {
        exit('<p class="cwp_error">Duplicate printer in JSON</p>');
    }
}

// return max file size allowed in a readable way
function GetMaxFileSize (array $settings)
{
    // @todo : tester aussi post_max_size
    $max_ini = ini_get("upload_max_filesize");

    // value can be of the form 2K, 2M or 2G, see
    // http://php.net/manual/en/faq.using.php#faq.using.shorthandbytes)
    if (!is_int($max_ini)) {
        $unit = substr($max_ini, -1);
        $max_ini = substr($max_ini, 0, -1);
        switch ($unit) {
            case "k":
            case "K":
                $max_ini = $max_ini * 1024;
                break;
            case "m":
            case "M":
                $max_ini = $max_ini *  1048576;
                break;
            case "g":
            case "G":
                $max_ini = $max_ini * 1073741824;
                break;
            default:
                exit('<p class="cwp_error">Variable upload_max_filesize
                    from php.ini does not seem correct.</p>');
        }
    }

    $max = min ($max_ini, $settings["max_file_size"] * 1024 * 1024);

    // bytes to MiB
    $max = $max / 1024 / 1024;
    // keep only 1 decimal
    if (!is_int($max)) {
        $max = number_format($max, 1);
    }

    return  $max . ' MiB';
}

// Language functions
/*====================================================================*/

// get preferred language from browser (2 or 3 letter code)
// adapted from http://www.albertcasadessus.com/2012/06/27/get-web-browser-preferrer-language-with-php-_server-variables-http_accept_language/
function GetLanguageCodeISO6391 (string $default_lang,
                                 bool $debug)
{
    if (!isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        $hi_code = $default_lang;

    } else {
        $hi_code = "";
        $hi_quof = 0;
        $langs = explode(",", $_SERVER['HTTP_ACCEPT_LANGUAGE']);

        foreach($langs as $lang) {
            // no quoficient written means that it is equal to "1" (=preferred)
            if (strpos($lang, ";") === false) {
                $lang .= ';q=1';
            }

            list($codelang,$quoficient) = explode(";q=", $lang);
            if($quoficient > $hi_quof) {
                // we keep only the language primary tag
                $primary_tag = explode("-", $codelang);
                $hi_code = $primary_tag[0];
                $hi_quof = $quoficient;
            }
        }
    }

    if ($debug === true) {
        echo '<p class="cwp_debug">Preferred language detected:'
            . $hi_code . '</p>';
    }

	return $hi_code;
}

// print UI string in the preferred language of the user if available
function L18nEcho (int $string_num,
                   string $string,
                   array $settings,
                   array $strings)
{
    $default_lang = $settings["default_lang"];
    $user_lang = $settings["user_lang"];

    if ($user_lang === "en") {
        echo $string;
    } elseif (isset($strings["interface"][$string_num][$user_lang])) {
        echo $strings["interface"][$string_num][$user_lang];
    } elseif (isset($strings["interface"][$string_num][$default_lang])) {
        echo $strings["interface"][$string_num][$default_lang];
    } else {
        echo $string;
    }
}

// retrieve name of a printer in the preferred language of the user if
// available
function GetPrinterNameString (array $printer,
                               array $settings)
{
    $default_lang = $settings["default_lang"];
    $user_lang = $settings["user_lang"];

    // we try to find printer name in the preferred language
    if (isset($printer["name"][$user_lang])) {
        return $printer["name"][$user_lang];
    } elseif (isset($printer["name"][$default_lang])) {
        // we try to find printer name for the default language
        return $printer["name"][$default_lang];
    } else {
        // we use cups printer name
        return $printer["cups-name"];
    }
}

// retrieve location of a printer in the preferred language of the user
// if available
function GetPrinterLocationString (array $printer,
                                   array $settings)
{
    $default_lang = $settings["default_lang"];
    $user_lang = $settings["user_lang"];

    // we try to find printer location in the preferred language
    if (isset($printer["location"][$user_lang])) {
        return $printer["location"][$user_lang];
    } elseif (isset($printer["location"][$default_lang])) {
        // we try to find printer location for the default language
        return $printer["location"][$default_lang];
    } else {
        return null;
    }
}

// retrieve print option in the preferred language of the user if
// available
function GetOptionString (string $option,
                          array $strings,
                          array $settings)
{
    $default_lang = $settings["default_lang"];
    $user_lang = $settings["user_lang"];
    // we try to find option name in the preferred language
    if (isset($strings["options"][$option]["name"][$user_lang])) {
        return $strings["options"][$option]["name"][$user_lang];
    } elseif (isset($strings["options"][$option]["name"][$default_lang])) {
        // we try to find option name for the default language
        return $strings["options"][$option]["name"][$default_lang];
    } else {
        // we return the option as named in CUPS
        return $option;
    }
}

// retrieve a value of a print option in the preferred language of the
// user if available
function GetOptionValueString (string $option,
                               string $value,
                               array $strings,
                               array $settings)
{
    $default_lang = $settings["default_lang"];
    $user_lang = $settings["user_lang"];
    // we try to find option name in the preferred language
    if (isset($strings["options"][$option]["values"][$user_lang][$value])) {
        return $strings["options"][$option]["values"][$user_lang][$value];
    } elseif (isset($strings["options"][$option]["values"][$default_lang][$value])) {
        // we try to find option name for the default language
        return $strings["options"][$option]["values"][$default_lang][$value];
    } else {
        // we return the option as named in CUPS
        return $value;
    }
}

/*====================================================================*/


// check a printer status
/*====================================================================*/
function GetPrinterStatus (string $printer,
                           string $server,
                           bool $debug)
{

    if ($server === "localhost") {
        $command = 'lpstat -E -p ' . $printer;
    } else {
        $command = 'lpstat -E -h ' . $server . ' -p ' . $printer;
    }

    exec(escapeshellcmd($command) . ' 2>&1', $output, $return_value);

    if ($debug === true) {
        echo '<p class="cwp_debug">Command: ' . $command
             . '<br>Return code: ' . $return_value . '<br>'
             . 'Output: ';
        foreach ($output as $line) {
            echo $line . '<br>';
        }
        echo '</p>';
    }

    if ($return_value !== 0) {
        $printerstatus["status"] = "commanderror";
        return $printerstatus;
    }

    // make one big string from output
    $outputstring = "";
    foreach ($output as $line) {
        $outputstring .= $line;
    }

    // some strange behaviour when server is localhost...
    if ($outputstring === "") {
        $printerstatus["status"] = "bug?";
        return $printerstatus;
    }

    // check if idle, printing, unavalaible, disabled or unknown
    // order is important
    if (strpos($outputstring, "Waiting for printer to become available") !== false) {
        $printerstatus["status"] = "unavailable";
    } elseif (strpos($outputstring, " disabled since ") !== false) {
        $printerstatus["status"] = "disabled";
    } elseif (strpos($outputstring, " is idle. ") !== false) {
        $printerstatus["status"] = "idle";
    } elseif (strpos($outputstring, " now printing ") !== false) {
        $printerstatus["status"] = "printing";
    } else {
        $printerstatus["status"] = "unknown";
    }

    // get number of jobs in queue
    if ($server === "localhost") {
        $command = 'lpq -E -P ' . $printer;
    } else {
        $command = 'lpq -E -h ' . $server . ' -P ' . $printer;
    }

    exec(escapeshellcmd($command) . ' 2>&1', $output, $return_value);

    if ($debug === true) {
        echo '<p class="cwp_debug">Command: ' . $command
             . '<br>Return code: ' . $return_value . '<br>'
             . 'Output: ';
        foreach ($output as $line) {
            echo $line . '<br>';
        }
        echo count($output);
        echo '</p>';
    }

    if ($return_value !== 0) {
        $printerstatus["jobsinqueue"] = "commanderror";
        return $printerstatus;
    }
    if (strpos($output[1], "no entries") !== false) {
        $printerstatus["jobsinqueue"] = 0;
    } else {
        $printerstatus["jobsinqueue"] = count($output) - 4;
    }

    return $printerstatus;
}



// create user forms
/*====================================================================*/
function GenForms (array $settings,
                   array $printers,
                   array $strings)
{
    // for each printer defined
    foreach ($printers as $printer) {

        // begin form output
        echo '<form action="." method="post" enctype="multipart/form-data">';

        // printer name and eventually link to CUPS status page
        $printer_string = GetPrinterNameString($printer, $settings);
        echo '<strong class="cwp_display-name">';
        if (isset($printer["link"])) {
            echo '<a href="' .  $printer["link"] . '">'
                . $printer_string . '</a>';
        } else {
            echo $printer_string;
        }
        echo  '</strong><br>';

        // location
        $location_string = GetPrinterLocationString($printer, $settings);
        if ($location_string !== null) {
            echo '<span class="cwp_location">' . $location_string . '</span><br>';
        }

        // eventually retrieve status
        if ($settings["retrieve_printers_status"] === true) {
            $printer_status = GetPrinterStatus($printer["cups-name"],
                                              $printer["server"],
                                              $settings["debug"]);

            // eventually print status
            if ($settings["show_printers_status"] === true) {
                echo '<span class="cwp_status">';

                switch ($printer_status) {
                    case "idle":
                        L18nEcho(0, 'Idle', $settings, $strings);
                        break;

                    case "unavailable":
                        L18nEcho(1, 'Unavailable (probably switched off or unplugged). Jobs in queue: ',
                             $settings, $strings);
                        echo ' ' . $printer_status["jobsinqueue"];
                        break;

                    case "disabled":
                        L18nEcho(2, 'Switched off or disabled. Jobs in queue: ',
                             $settings, $strings);
                        echo ' ' . $printer_status["jobsinqueue"];
                        break;

                    case "printing":
                        L18nEcho(3, 'Printing. Next jobs in queue: ', $settings,
                             $strings);
                        echo ' ' . $printer_status["jobsinqueue"] - 1;
                        break;

                    case "commanderror":
                        L18nEcho(4, 'Could not get printer status: print server might be down.',
                             $settings, $strings);
                        case "bug?":
                        break;

                    default:
                        L18nEcho(6, 'Unknown status', $settings, $strings);
                }

                echo '</span><br>';
            }

            // eventually close the form and stops here
            if ($settings["disable_faulty_printers"] === true) {
                if ($printerstatus["status"] === "unavailable"
                    || $printerstatus["status"] === "disabled"
                    || $printerstatus["status"] === "commanderror") {
                    echo '</form>' . PHP_EOL;
                    continue;
                }
            }
        }

        // options
        if (isset($printer["options"]) && ! empty ($printer["options"])) {

            // for each option
            foreach ($printer["options"] as $option_name => $option) {

                $option_string = GetOptionString($option_name,
                                                 $strings,
                                                 $settings);

                // detect the type of option (text field, list or checkbox)
                if (isset($option["values"]) && $option["values"] !== "bool") {

                    // list
                    echo '  ' . $option_string . ': <select name="'
                         . $option_name . '"';
                    if (isset($option["changeable"])
                        && $option["changeable"] === false) {
                        echo ' disabled ';
                    }
                    echo '>' . PHP_EOL;
                    foreach($option["values"] as $value) {
                        $value_string = GetOptionValueString($option_name,
                                                             $value,
                                                             $strings,
                                                             $settings);
                        echo '    <option value="' . $value . '">'
                             . $value_string . '</option>' . PHP_EOL;
                    }
                    echo '  </select> <br>' . PHP_EOL;

                } elseif (isset($option["values"])
                          && $option["values"] === "bool") {

                    // checkbox
                    echo '  ' . $option_string
                         . ': <input type="checkbox" name="'
                         . $option_name . '" value="true"';
                    if (isset($option["changeable"])
                        && $option["changeable"] === false) {
                        echo ' disabled';
                    }
                    if (isset($option["default"])
                        && $option["default"] === true) {
                        echo ' checked';
                    }
                    echo '> <br>' . PHP_EOL;
                    // if it is disabled but checked we need to create a hidden
                    // input to still get the value...
                    if (isset($option["changeable"])
                        && $option["changeable"] === false
                        && isset($option["default"])
                        && $option["default"] === true) {
                        echo '  <input type="hidden" name="' . $option_name
                             . '" value="true" checked>' . PHP_EOL;
                    }

                } else {

                    // text field
                    echo '  ' . $option_string . ': <input type="text" name="'
                         . $option_name . '"';
                    if (isset($option["changeable"])
                        && $option["changeable"] === false) {
                        echo ' disabled';
                    }
                    if (isset($option["default"])) {
                        echo ' value="' . $option["default"] . '"';
                    }
                    echo '> <br>' . PHP_EOL;
                }
            }
        }

        // hidden fields to identify the printer chosen by user
        echo '<input type="hidden" name="cups-name" value="'
             .  $printer["cups-name"] . '">' . PHP_EOL;
        echo '<input type="hidden" name="server" value="'
             . $printer["server"] . '">' . PHP_EOL;
        // hidden field to retrieve displayed name easily
        echo '  <input type="hidden" name="display-name" value="'
             . $printer_string . '">' . PHP_EOL;

        // file selector and end of the form of this printer
        echo '<input type="hidden" name="MAX_FILE_SIZE" value="'
            . ($settings["max_file_size"] * 1024 * 1024) . '" />';
        L18nEcho(7, 'File:', $settings, $strings);
        echo '<input type="file" accept=".pdf" name="filename" />';
        echo '<input type="submit" value="';
            L18nEcho(8, 'Print', $settings, $strings);
        echo '" />' . PHP_EOL . '</form>' . PHP_EOL;
    }
}

// check $_FILES
/*====================================================================*/
function IsFILESValid(array $settings,
                      array $strings) : bool
{
    // eventually handle file upload error
    $phpFileUploadErrors = array(
        1 => 'file size exceeds server limit',
        2 => 'file size exceeds authorized limit',
        3 => 'file was not fully uploaded.',
        4 => 'no file was selected.',
        6 => 'missing a temporary folder.',
        7 => 'failed to write file to disk.',
        8 => 'a PHP extension stopped the file upload.',
    );

    if ($_FILES["filename"]["error"] !== 0) {
        echo '<p class="cwp_error">';
        L18nEcho(9, 'Error:', $settings, $strings);
        echo ' ';
        L18nEcho(($_FILES["filename"]["error"] + 50),
                 $phpFileUploadErrors[$_FILES["filename"]["error"]],
                 $settings, $strings);
        if ($_FILES["filename"]["error"] === 1
            || $_FILES["filename"]["error"] === 2) {
            echo ' (' . GetMaxFileSize($settings) . ')';
        }
        echo '</p>' . PHP_EOL;
        return false;

    } elseif (!is_uploaded_file($_FILES["filename"]["tmp_name"])
              || !isset($_FILES["filename"]["error"])) {
        echo '<p class="cwp_error">';
        L18nEcho(9, 'Error:', $settings, $strings);
        echo ' ' . $_FILES['filename']['error'] . ': ';
        L18nEcho(11, 'incorrect parameters', $settings,
                 $strings);
        echo '</p>' . PHP_EOL;
        return false;

    } elseif ($settings["max_file_size"] !== "0"
        && $_FILES["filename"]["size"] > ($settings["max_file_size"] * 1024 * 1024)) {
        echo '<p class="cwp_error">';
        L18nEcho(9, 'Error:', $settings, $strings);
        echo ' ';
        L18nEcho(52, $phpFileUploadErrors[2], $settings, $strings);
        echo ' (' . GetMaxFileSize($settings) . ')';
        echo '</p>' . PHP_EOL;
        return false;
    }

    // check file type
    $filetype = exec('file --brief --mime ' .
                     escapeshellarg($_FILES['filename']['tmp_name']));
    if (strpos($filetype, "application/pdf") === false) {
        echo '<p class="cwp_error">';
        L18nEcho(9, 'Error:', $settings, $strings);
        echo ' ';
        L18nEcho(12, 'file is not in PDF format.', $settings, $strings);
        echo '</p>' . PHP_EOL;
        return false;
    }

    return true;
}

// check $_POST (= $user_input)
// this should never fail with a honnest user
/*====================================================================*/
function IsPOSTValid(array $printers,
                     array $user_input) : bool
{
    $printer_exists = false;

    foreach ($printers as $printer) {
        // try to find the corresponding printer
        if ($user_input["cups-name"] === $printer["cups-name"]
            && $user_input["server"] === $printer["server"]) {

            $printer_exists = true;

            // check display-name
            if (!$user_input["display-name"] === $printer["cups-name"]) {
                if (!array_search($user_input["display-name"], $printer["name"], true)) {
                    return false;
                }
            }

            unset($user_input["cups-name"]);
            unset($user_input["server"]);
            unset($user_input["display-name"]);

            // check options
            foreach ($user_input as $option => $value) {

                // option must exist
                if (!array_key_exists($option, $printer["options"])) {
                    return false;
                }

                // value must be set to one available
                if (isset($printer["options"][$option]["values"])
                    && $printer["options"][$option]["values"] !== "bool") {
                    if (array_search($value, $printer["options"][$option]["values"], true) === false) {
                        return false;
                    }
                }

                // boolean option must be set to a boolean value
                if (isset($printer["options"][$option]["values"])
                    && $printer["options"][$option]["values"] === "bool") {
                    if ($value !== "true" && $value !== "false") {
                        return false;
                    }
                }

                // unchangeable value must be set to default
                if (isset($printer["options"][$option]["changeable"])
                    && $printer["options"][$option]["changeable"] === false) {
                    if ($value != $printer["options"][$option]["default"]) {
                        return false;
                    }
                }
            }

            // printer exists and options are valid
            break;
        }
    }

    if ($printer_exists === true) {
        return true;
    } else {
        return false;
    }
}

/*====================================================================*/

// check known options that have text input
/*====================================================================*/
function CheckUserStrings(array $settings,
                          array $strings,
                          array $user_input)
{
    // number of copies
    if (isset($user_input["copies"])) {
        if (!filter_var($user_input["copies"], FILTER_VALIDATE_INT)
            || $user_input["copies"] < 1) {
            echo '<p class="cwp_error">';
            L18nEcho(13, 'Number of copies is not valid.', $settings,
                     $strings);
            echo '</p>' . PHP_EOL;
            return false;
        } elseif ($settings["max_copies"] !== "0"
                  && $user_input["copies"] > $settings["max_copies"]) {
            echo '<p class="cwp_error">';
            L18nEcho(14, 'Number of copies is limited to', $settings,
                     $strings);
            echo ' ' . $settings["max_copies"] . '</p>' . PHP_EOL;
            return false;
        }
    }

    // pages to print
    if (isset($user_input["page-list"])
        && $user_input["page-list"] !== "") {

        // basic syntax check
        if (!preg_match('/^\d+((\d|-|,)\d+)*$/', $user_input["page-list"])) {
            echo '<p class="cwp_error">';
            L18nEcho(17, 'Bad page ranges format. Example: 1,3-5,16',
                     $settings, $strings);
            echo '</p>' . PHP_EOL;
            return false;
        }

        $ranges = explode(',', $user_input["page-list"]);

        // quite ugly, but working and understandable
        $pages = array();
        foreach ($ranges as $range) {
            // page alone
            if (filter_var($range, FILTER_VALIDATE_INT)) {
                array_push($pages, $range);
            // range of pages
            } else{
                list($first, $last) = explode('-', $range);
                if ($first >= $last) {
                    echo '<p class="cwp_error">';
                    L18nEcho(17, 'Bad page ranges format. Example: 1,3-5,16',
                             $settings, $strings);
                    echo '</p>' . PHP_EOL;
                    return false;
                } else {
                    for ($page_number = $first; $page_number <= $last; $page_number++) {
                        array_push($pages, $page_number);
                    }
                }
            }
        }

        if (count(array_unique($pages)) < count($pages)) {
            echo '<p class="cwp_error">';
            L18nEcho(17, 'Bad page ranges format. Example: 1,3-5,16',
                     $settings, $strings);
            echo '</p>' . PHP_EOL;
            return false;
        }
    }

    return true;
}

// print
/*====================================================================*/
function PreparePrinting (array $settings,
                          array $strings,
                          array $user_input)
{
    // beginning of the command
    $command = 'lp -E';
    // eventually sets the CUPS server
    if ($user_input["server"] !== "localhost") {
        $command .= ' -h ' . $user_input["server"];
    }
    // sets printer
    $command .= ' -d ' . $user_input["cups-name"];
    // sets job name with quoting
    $command .= ' -t "' . $_FILES["filename"]["name"] . '"';

    // eventually sets the username, depending if user is logged in or not
    if (isset($settings["username"]) && $settings["username"] !== "") {
        $command .= ' -U ' . $cwp_username;
    } elseif (isset($_SERVER["PHP_AUTH_USER"])
              && $_SERVER["PHP_AUTH_USER"] !== "") {
        $command .= ' -U ' . $_SERVER["PHP_AUTH_USER"];
    } elseif (isset($settings["default_username"])
              && $settings["default_username"] !== ""){
        $command .= ' -U ' . $settings["default_username"];
    }

    // special parameter: number of copies
    if (isset($user_input["copies"])) {
        $command .= ' -n ' . $user_input["copies"];
    }

    // special parameter: pages to print
    if (isset($user_input["page-list"])
        && $user_input["page-list"] !== "") {
        $command .= ' -P ' . $user_input["page-list"];
    }

    // unsets parameters we don't need anymore
    unset($user_input["cups-name"]);
    unset($user_input["server"]);
    unset($user_input["display-name"]);
    unset($user_input["copies"]);
    unset($user_input["page-list"]);

    // all other options
    foreach ($user_input as $option => $value) {
        // boolean options do not need value
        if ($value === "true") {
            $command .= ' -o ' . escapeshellarg($option);
        } else {
            // other options need value
            $command .= ' -o ' . escapeshellarg($option) . '=' . $value;
        }
    }

    // adds filename with quoting
    $command .= ' ' . escapeshellarg($_FILES['filename']['tmp_name']);

    exec(escapeshellcmd($command) . ' 2>&1', $output, $return_value);

    if ($settings["debug"] === true) {
        echo '<p class="cwp_debug">Command sent to CUPS: ' . $command
             . '<br>'
             . 'Return code: ' . $return_value . '<br>'
             . 'Output: ';
        foreach ($output as $line) {
            echo $line . '<br>';
        }
        echo '</p>';
    }

    // if lp was OK
    if ($return_value === 0) {

        echo '<p class="cwp_success">';
        L18nEcho(15, 'File', $settings, $strings);
        echo ' <strong>' . $_FILES['filename']['name'] . '</strong> ';
        L18nEcho(16, 'has been sent to print queue of', $settings,
                 $strings);
        echo ' <strong>' . $_POST["display-name"] . '</strong></p>';

    // if lp was not OK
    } else {

        echo '<p class="cwp_error">';

        // handling error that need an admin to be called
        // example : handle lp: Error - scheduler not responding.
        if ($output[0] === "lp: Bad file descriptor") {
            L18nEcho(19, 'Print server is probably down or unreachable.',
                        $settings, $strings);
            echo '<br>';
        } else {
            // when error is unknown
            L18nEcho(18, 'An error occured:', $settings, $strings);
            echo ' <em>';
            foreach ($output as $line) {
                echo $line . '<br>';
            }
            echo '</em>';
        }

        L18nEcho(20, 'Please contact the administrator.', $settings,
                 $strings);

        echo '</p>' . PHP_EOL;
    }
}
?>
