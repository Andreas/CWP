<?php

return [
    // file describing printers and available options
    'printers_file' => './config/printers-template.php',
    // file containing strings and translations of CWP interface and
    // printer options
    'strings_file' => './config/cwp-strings.php',
    // default language to use when no language is preferred by browser or
    // when no translation is available (2 or 3 letters code)
    'default_lang' => 'en',
    // retrieve printers status
    'retrieve_printers_status' => true,
    // display status and number of jobs in queue for each printer
    'show_printers_status' => true,
    // enable printing only on available printers
    'disable_faulty_printers' => false,
    // maximum number of copies for one job. Set to '0' to disable.
    'max_copies' => 50,
    // maximum size of the file to print (in MiB, 1 048 576 bytes is 1MiB)
    // to be more restrictive than global PHP config. Set to '0' to disable
    'max_file_size' => 5,
    // string used as job username if user is not authenticated.
    'default_username' => 'cwp',
    // print some debug output
    'debug' => false,
];