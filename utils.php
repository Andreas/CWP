<?php

// check if a "server" parameter is valid or not
function isValidServerUri ($server)
{
    // eventually check and extract port number
    $splitted = preg_split('/:(?!.*:)/', $server);

    // if ":" has been found and if it not part of an IPv6
    if (count($splitted) === 2
        && !preg_match('/]/', end($splitted))) {

        // if it is not a valid port number
        if (is_numeric(end($splitted))) {

            if (!filter_var(end($splitted), FILTER_VALIDATE_INT)
                || end($splitted) < 1
                || end($splitted) > 65534) {
                return false;
            // else we remove it from the string
            } else {
                $server = preg_replace('/:\d{1,5}$/', "", $server);
            }

        } else {
            return false;
        }
    }

    // IPv6 address
    if (preg_match('/^\[.+\]$/', $server)) {

        $server = preg_replace('/(^\[)|(\]$)/' , "", $server);

        // address check
        if (filter_var($server, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 !== false)) {
            return true;
        } else {
            return false;
        }

    // IPv4 address
    } elseif (filter_var($server, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 !== false)) {
        return true;
    }

    // if it is a domain name
    // thanks to velcrow http://stackoverflow.com/questions/1755144/how-to-validate-domain-name-in-php#4694816
    // 3 checks :
    // - valid chars
    // - overall length
    // - length of each label
    if (preg_match('/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i', $server)
        && preg_match('/^.{1,253}$/', $server)
        && preg_match('/^[^\.]{1,63}(\.[^\.]{1,63})*$/', $server)) {
        return true;
    } else {
        return false;
    }
}

function validateRequired($field): string {
    return (!isset($field) || $field === '') ? _('is required') : '';
}

function validateBoolean($field): string {
    return !is_bool($field) ? _('must be a boolean') : '';
}

function validateInteger($field): string {
    return !is_int($field) ? _('must be an integer') : '';
}

function validateString($field): string {
    return !is_string($field) ? _('must be a string') : '';
}

function validateUrl($field): string {
    return filter_var($field, FILTER_VALIDATE_URL) === false ? _('must be a valid url') : '';
}

function validateRegex($field, $pattern): string {
    return preg_match($pattern, $field) !== 1 ? _('does not match regex') . ' ' . $pattern : '';
}

function validateMin($field, $min): string {
    return $field < $min ? _('must be at least') . ' ' . $min : '';
}

function validateCount($field, $count): string {
    return count($field) != $count ? _('count must be') . ' ' . $count : '';
}

function validateMinCount($field, $count): string {
    return count($field) < $count ? _('count must be at least') . ' ' . $count : '';
}

function validateIndexed($field): string {
    return (array_unique(array_map("is_int", array_keys($field))) !== array(true)) ? _('must be an indexed array, not associative') : '';
}

function validateAssociative($field): string {
    // Accept empty array
    if (count($field) === 0) {
        return '';
    }
    return (array_unique(array_map("is_string", array_keys($field))) !== array(true)) ? _('must be an associative array, not an indexed') : '';
}

function validatePrinterName($field): string {
    // see man page of lpadmin
    // TODO could not find a regex to validate "all printable unicode
    // characters but spaces, "#" and "/"
    return false ? _("is not a valid printer's name") : '';
}

function validateServerUri($field): string {
    return isValidServerUri($field) !== true ? _('is not a valid server uri') : '';
}

function validateField($key, $field, $rules_str): string {
    // Rule can also be a anonymous function
    if (gettype($rules_str) === 'object') {
        return $rules_str($field);
    }
    $error = '';
    // Based on this Stackoverflow anwser :
    // stackoverflow.com/questions/6462578/alternative-to-regex-match-all-instances-not-inside-quotes
    $rules_array = preg_split('/\|(?=([^\/]*\/[^\/]*\/)*[^\/]*$)/', $rules_str);
    foreach ($rules_array as $rule) {
        $rule_exploded = explode(':', $rule);
        $ruleName = $rule_exploded[0];
        $rulePattern = count($rule_exploded) > 1 ? $rule_exploded[1] : '';
        $validatorName = 'validate' . ucwords($ruleName);
        if(function_exists($validatorName)) {
            if($ruleName === 'required') {
                $error = call_user_func($validatorName, $field, $rulePattern);
            }
            else if(isset($field)) {
                $error = call_user_func($validatorName, $field, $rulePattern);
            }
            if($error !== '') {
                return $key . ' ' . $error . ', ' . $field . ' '. _('given');
            }
        }
    }
    return $error;
}
// Inspired by Laravel's validation format (https://laravel.com/docs/5.3/validation)
function validateArray(array $array_field, array $validation_rules): array {
    $errors = [];

    foreach ($validation_rules as $key => $rules_str) {
        // It is possible to validate all keys or values of an array
        $exploded_key = explode('|', $key);
        $field_key = $exploded_key[0];
        // If no field specified, then take the entire array for validation
        $field = null;
        if ($field_key === '') {
            $field = $array_field;
        } elseif (array_key_exists($field_key, $array_field)) {
            $field = $array_field[$field_key];
        }
        //TODO add errors if the field is not an array
        if(count($exploded_key) > 1 && is_array($field)) {
            $elements_to_validate = $exploded_key[1];

            foreach ($field as $k => $v) {
                //TODO add error if elements_to_validate != 'keys' or != 'values'
                if ($elements_to_validate === 'keys') {
                    $err = validateField($k, $k, $rules_str);
                } elseif ($elements_to_validate === 'values') {
                    $err = validateField($k, $v, $rules_str);
                }
                if ($err !== '') {
                    $errors[$field_key . '.' . $k] = $err;
                }
            }

        } else {
            $err = validateField($field_key, $field, $rules_str);
            if($err !== '') {
                $errors[$field_key] = $err;
            }
        }
    }
    return $errors;
}