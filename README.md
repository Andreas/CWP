CWP: CUPS Web Printing
======================
Copyright 2016, Yvan Masson (yvan.masson@openmailbox.org)
This file is part of CWP. See the end of the file for license
information.


Warning
------------------------------------------------------------------------
This code, although working, is still experimental and certainely
contains bugs and security issues: do not use it in production.

Major known issues:
- strings coming from configuration files are not escaped when printed
- there is nothing to protect your web and CUPS server against flooding


Introduction
------------------------------------------------------------------------
CWP is a PHP web page to allow printing PDF files against CUPS printers.
It has been briefly on Debian but should work on any modern Unix system.

It can be useful for exemple to allow guests to use their own computer
to print: they do not need to configure the printers driver, and do not
need access to the CUPS server.

Features:
- CUPS server can be remote
- any CUPS and printer option can be defined by admin and chosen by user
- default value can be set for each option
- settings can be locked (displayed but not changeable)
- job username can be set to anything (see "user authentication" below)
- printer status can be displayed
- you can prevent printing against unavailable printers
- size of the file and number of copies can be restricted
- fully translatable (English and French are available yet)
- can be included in another web page
- a debug mode displays command sent to CUPS

Requirements :
- web server with PHP allowing "exec" function (tested with Apache 2.4
  and PHP 7.0)
- "mbstring" PHP functions (package php-mbstring on Debian)
- "file" command (tested with file 5.28)
- "lp", "lpstat" and "lpq" commands from standard CUPS installation
  (tested with CUPS 2.1.4)

I would be happy that you let me know for any comment about this code,
either bad or good.


Installation
------------------------------------------------------------------------
Files must be put in a folder served by a web server with PHP. Standard
PHP extensions are sufficient (at least in Debian).

You should check and set variables in index.php. You can also include
the PHP code of this file in your own webpage (just make sure that the
links to functions file, JSON and CSS are still correct).


Printers definition
------------------------------------------------------------------------
Printers available to users are by default defined in the JSON file
"printers-template.json". It is advised that you use a copy of this
file, so that you can keep the original for reference. Run CWP with this
template for fast understanding or read the following for more details.

JSON format is well described on http://www.json.org/. It is easy to
forget a "," or a "}", but you can get help with a JSON validator such
as http://www.json.fr/ (many thanks for this).

"cups-name" is the name of the printers within CUPS, and can be found
from the web interface or with "lpstat -s".

Use whatever you want for "name" and "location".

"server" must be set, even for localhost CUPS server, and can be a DNS
name or an IP address. IPv6 address should be enclosed by "[" and "]"
(ex.: [::1]). Port number can be added at the end, precedeed by ":".

If "link" is set, a link will be displayed for this printer. Usual usage
is a link to the CUPS server web interface to see pending jobs.

There are two categories of printer options:
- Generic: listed in "lp" manpage.
- Specific to the printer: shown using "lpoptions -p printer_name -l"

If you do not specify an option, CUPS will use the default value.

Options will be displayed to the user in the order you write them.

If it has no "value", the option will be a text field. If "value" is an
array of values, the option will be a "select list", the first value
being the default. If "value" is set to "bool", it will output a
checkbox (you should not need this often appart from the "fit-to-page"
option).

If "changeable" is set to "false" (without the quotes), the setting will
appear but the user will not be able to change it.

"default" value can be set for text fields and checkbox. For checkboxes,
it must be "true" or "false" (without the quotes).


User authentication / authorization
------------------------------------------------------------------------
CWP does not provide authentication and authorization.

By default, the username in the lp command line will be set to the value
of $settings["default_username"]. If it is empty or not set, username
will be set with the user running PHP.

This will be overridden by the value of $_SERVER['PHP_AUTH_USER'] if it
is set (for example if you use authentication and authorization provided
by Apache, either basic
(https://httpd.apache.org/docs/current/howto/auth.html) or LDAP
(https://httpd.apache.org/docs/current/mod/mod_authnz_ldap.html).

If you need another username, you can write your own PHP code to fill in
the variable $settings["username"], and it will be used instead.


Strings and translations
------------------------------------------------------------------------
Options strings and translations are provided in cwp-strings.json. You
can add string for specific printer option.

Please send me patches if you can translate it to another language.


License
------------------------------------------------------------------------
CWP is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CWP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with CWP. If not, see <http://www.gnu.org/licenses/>.
